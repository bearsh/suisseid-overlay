# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit eutils unpacker

MY_PV="6.21.78~jessie.1"
MY_P="${PN}_${MY_PV}"

DESCRIPTION="PKCS#11 Library for SuisseID"
HOMEPAGE="https://postsuisseid.ch"
SRC_URI_COMMON="http://update.swisssign.com/media/stick/repository/dists/jessie/non-free/binary-"
SRC_URI="amd64? ( ${SRC_URI_COMMON}amd64/${MY_P}_amd64.deb )
	x86? ( ${SRC_URI_COMMON}i386/${MY_P}_i386.deb )
"

LICENSE="suisseid"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RESTRICT="mirror"
QA_PREBUILT="*"

RDEPEND="app-crypt/acsccid
	sys-apps/pcsc-lite
	!dev-libs/openct
	!dev-libs/opensc
"
DEPEND="${RDEPEND}"

S=${WORKDIR}/

src_install() {
	into /usr
	dolib.so usr/lib/*

	insinto /usr/share/doc/${PF}
	doins -r usr/share/doc/${PN}/*
}
