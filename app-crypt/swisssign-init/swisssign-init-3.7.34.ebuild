# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit eutils unpacker

MY_PV="3.7.34~jessie.522"
MY_P="${PN}_${MY_PV}"

DESCRIPTION="SuisseID Assistant - Initialize SuisseID Smartcard"
HOMEPAGE="https://postsuisseid.ch"
SRC_URI_COMMON="http://update.swisssign.com/media/stick/repository/dists/jessie/non-free/binary-"
SRC_URI="amd64? ( ${SRC_URI_COMMON}amd64/${MY_P}_amd64.deb )
	x86? ( ${SRC_URI_COMMON}i386/${MY_P}_i386.deb )
"

LICENSE="suisseid"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

RESTRICT="mirror"
QA_PREBUILT="*"

RDEPEND="app-crypt/acsccid
	app-crypt/suisseid-pkcs11
	app-crypt/p11-kit
	dev-libs/libpcscxx
	dev-libs/openssl:0
	dev-libs/proxyface
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtnetwork:5
	dev-qt/qtwidgets:5
	net-libs/libproxy
	sys-apps/pcsc-lite
	x11-libs/libX11
"
DEPEND="${RDEPEND}"

S=${WORKDIR}/

src_prepare() {
	default
	if ! use doc; then
		rm -Rf usr/share/doc/${PN}/html
	fi
}

src_install() {
	into /usr
	dobin usr/bin/commandline-assistant
	dobin usr/bin/swisssign-init

	domenu usr/share/applications/swisssign-init.desktop

	insinto /usr/share
	doins -r usr/share/${PN}

	insinto /usr/share/doc/${PF}
	doins -r usr/share/doc/${PN}/*
}
