# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="SuisseID Meta Packet"
LICENSE="metapackage"
HOMEPAGE="https://postsuisseid.ch"
KEYWORDS="~amd64 ~x86"
SLOT=0

RDEPEND="app-crypt/swisssign-init
	 app-crypt/suisseid-configuration
	 app-crypt/swisssigner
"
