# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

##inherit autotools #multilib-minimal

DESCRIPTION="MRW C++ Library"
HOMEPAGE="https://dev.marc.waeckerlin.org/redmine/projects/${PN}"
SRC_URI="https://dev.marc.waeckerlin.org/repository/sources/${PN}/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="examples doc static-libs"

RDEPEND="dev-libs/boost:=[threads]
	dev-libs/libltdl
	dev-libs/log4cxx
	sys-libs/binutils-libs:0=
	sys-libs/zlib
"
DEPEND="${RDEPEND}
	doc? (
		app-doc/doxygen
		media-gfx/graphviz
		media-gfx/mscgen
	)
"

src_prepare() {
	if ! use doc; then
		sed -i -e '/SUBDIRS =/ s/doc//' makefile.in || die "sed failed"
	fi
	if ! use examples; then
		sed -i -e '/SUBDIRS =/ s/examples//' makefile.in || die "sed failed"
	fi
	default
}

src_configure() {
	local econfargs=(
		--enable-shared
		$(use_enable static-libs static)
	)
	econf "${econfargs[@]}"
}

src_install() {
	default
	prune_libtool_files --all
}
