# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit eutils flag-o-matic

DESCRIPTION="C++ Library for accessing PCSC-lite, OpenSSL, PKCS#11"
HOMEPAGE="https://dev.marc.waeckerlin.org/redmine/projects/${PN}"
SRC_URI="https://dev.marc.waeckerlin.org/repository/sources/${PN}/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc static-libs"

RDEPEND="app-crypt/p11-kit
	dev-libs/mrw-c++
	dev-libs/openssl:0
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtnetwork:5
	dev-qt/qtwidgets:5
	sys-apps/pcsc-lite
"
DEPEND="${RDEPEND}
	doc? (
		app-doc/doxygen
		media-gfx/graphviz
		media-gfx/mscgen
	)
"

src_prepare() {
	if ! use doc; then
		sed -i -e '/SUBDIRS =/ s/doc//' makefile.in || die "sed failed"
	fi
	# examples don't build atm, not needed anyway
	sed -i -e '/SUBDIRS =/ s/examples//' makefile.in || die "sed failed"
	# fix configure script to rather look for TOOL then TOOL-qt5
	for tool in qmake moc uic rcc lupdate lrelease; do
		sed -i -e "/${tool^^}/ s/${tool}-qt5/${tool}/" configure || die "sed failed"
	done
	default
}

src_configure() {
	local econfargs=(
		--enable-shared
		$(use_enable static-libs static)
	)
	append-cppflags "-D_GLIBCXX_USE_CXX11_ABI=0"
	econf "${econfargs[@]}"
}

src_install() {
	default
	prune_libtool_files --all
}
