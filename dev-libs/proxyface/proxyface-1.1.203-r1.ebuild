# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit eutils flag-o-matic

DESCRIPTION="Platform Independent Interface for Network Proxies"
HOMEPAGE="https://dev.marc.waeckerlin.org/redmine/projects/${PN}"
SRC_URI="https://dev.marc.waeckerlin.org/repository/sources/${PN}/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

RDEPEND="dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtnetwork:5
	dev-qt/qtwidgets:5
	net-libs/libproxy
"
DEPEND="${RDEPEND}
	doc? (
		app-doc/doxygen
		media-gfx/graphviz
		media-gfx/mscgen
	)
"

src_prepare() {
	if ! use doc; then
		sed -i -e '/SUBDIRS =/ s/doc//' makefile.in || die "sed failed"
	fi
	# not needed...
	sed -i -e '/SUBDIRS =/ s/examples//' makefile.in || die "sed failed"
	default
}

src_configure() {
	local econfargs=(
		--enable-shared
		--disable-static
	)
	append-cppflags "-D_GLIBCXX_USE_CXX11_ABI=0"
	econf "${econfargs[@]}"
}

src_install() {
	default
	prune_libtool_files --all
}
